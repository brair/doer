<?= "<?php\n"; ?>

declare(strict_types=1);

namespace <?= $namespace; ?>;

use App\Aplication\Command\<?= $command_for_handler; ?>;
use Symfony\Component\Console\Command\Command;
use League\Tactician\CommandBus;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class <?= $class_name; ?> extends Command
{
    protected static $defaultName = '<?= $command_name; ?>';
    private $commandBus;

    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        //$command = new <?= $command_for_handler; ?>();
        //$this->commandBus->handle($command);
    }
}
