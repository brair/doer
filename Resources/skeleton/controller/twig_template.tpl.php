<?= $helper->getHeadPrintCode("Hello $class_name!"); ?>

{% block body %}
<div>
    <h1>Hello {{ controller_name }}! ✅</h1>
</div>
{% endblock %}
