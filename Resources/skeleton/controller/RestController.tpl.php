<?= "<?php\n" ?>

namespace <?= $namespace; ?>;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

class <?= $class_name; ?> extends AbstractController
{
    /**
     * @Route("<?= $route_path ?>", name="<?= $route_name ?>")
     */
    public function index(): JsonResponse
    {
        return $this->json([
            'message' => 'Welcome to your new rest controller!'
        ]);
    }
}
