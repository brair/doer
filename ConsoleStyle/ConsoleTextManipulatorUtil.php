<?php

declare(strict_types=1);

namespace Brair\Doer\ConsoleStyle;

use Symfony\Bundle\MakerBundle\ConsoleStyle;

class ConsoleTextManipulatorUtil
{
    public function paintTextBackground(array $setOfText, string $color): array
    {
        foreach ($setOfText as $line) {
            $text[] = '<bg=' . $color . ';fg=white>' . $line . '</>';
        }

        return $text ?? [];
    }

    public function setTextLineSameLength(array $setOfText): array
    {
        $computedLine = [];

        foreach ($setOfText as $line) {
            $lineDimension['numberOfChars'] = mb_strlen($line);
            $lineDimension['text'] = $line;
            $numberOfCharacters[] = mb_strlen($line);

            $computedLine[] = $lineDimension;
        }

        $maxLen = max($numberOfCharacters ?? 0);

        foreach($computedLine as $lineDimension) {
            $lineDimension['missingChars'] = $maxLen - $lineDimension['numberOfChars'];
            $lineDimension['text'] .= str_repeat(" ", $lineDimension['missingChars']);
            $resultArray[] = $lineDimension['text'];
        }

        return $resultArray ?? [];
    }


    /**
     * EXAMPLE USAGE SHORTCUT: php bin/console make:command-handler -e=exampleName
     * EXAMPLE USAGE IN CODE: $foo = $this->removeEqualSignFromShortcut($input->getOption('example-name'));
     */
    public function removeEqualSignFromShortcut(?string $param)
    {
        if (!$param) return null;

        if (is_array($param)) {
            $firstParam = trim($param[0]);

            if (substr($firstParam, 0, 1) == "=") {
                $param[0] = substr($firstParam, 1);
                return $param;
            }
        } else {
            $param = trim($param);
            if (substr($param, 0, 1) == "=") {
                return substr($param, 1);
            }
        }

        return $param;
    }

    public function writeSuccessMessage(ConsoleStyle $io)
    {
        $io->newLine();
        $io->writeln(' <bg=green;fg=white>          </>');
        $io->writeln(' <bg=green;fg=white> Success! </>');
        $io->writeln(' <bg=green;fg=white>          </>');
        $io->newLine();
    }


    public function writeCentredMessage(string $message, string $longestText, ConsoleStyle $io): void
    {
        $maxCharsMessage = mb_strlen($message);
        $addChars = (mb_strlen($longestText) - $maxCharsMessage)/3;
        $computedMessage = str_repeat(" ", (int)$addChars);
        $computedMessage .= $message;

        $io->text([$computedMessage]);
    }
}
